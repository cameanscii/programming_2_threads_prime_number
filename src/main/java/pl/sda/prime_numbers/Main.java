package pl.sda.prime_numbers;

import java.io.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    private static AtomicInteger numberOfPrimeNumbers=new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
        isPrimeFileReader();
    }


    public static boolean isPrimeNumber(long number) {
        if (number<=1) return false;
        if(number==2) return true;
        if(number%2==0)return false;

        for (int i = 3; i < number; i+=2) {
            if (number % i == 0)
                return false;
        }
        return true;
    }


    public static void isPrimeFileReader() throws InterruptedException {
        long startTime = System.currentTimeMillis();

        ExecutorService threadPool =Executors.newFixedThreadPool(4);
        File file = new File("src\\main\\resources\\numbers.txt");

        final CountDownLatch latch=new CountDownLatch(5000);
        try (BufferedReader reader=new BufferedReader(new FileReader(file))){
            String linia;

            while ((linia = reader.readLine()) != null) {
                final long number=Long.parseLong(linia);
                threadPool.submit(new Runnable() {
                    @Override
                    public void run() {
                        if(isPrimeNumber(number)){
                            numberOfPrimeNumbers.incrementAndGet();
                        }
                        latch.countDown();
                    }
                });
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        latch.await();
        threadPool.shutdown();
        long stopTime = System.currentTimeMillis();
        System.out.println("In file is: "+ numberOfPrimeNumbers+" prime numbers");
        System.out.println((stopTime-startTime)+" ms");
    }
}






